sap.ui.controller("cus.ESAN.SOGirisOnay.view.Master", {
    onInit: function() {
        this.oView = this.getView();
        this.oComponent = sap.ui.component(sap.ui.core.Component.getOwnerIdFor(this.oView));
        this.oResourceBundle = this.oComponent.getModel(this.oComponent.i18nModelName).getResourceBundle();
        this.oRouter = this.oComponent.getRouter();
        this.oRouter.attachRoutePatternMatched(this.onRoutePatternMatched, this); 
        this.busyCount = 0;
        this.checkOdata = false;
    },     
    onRoutePatternMatched: function(oEvent) { 
    	var t = this;
    	this.searchFragmentId = "searchFragment";
        this.sPageName = oEvent.getParameter("name");
        this.filterFragmentId = "filterCbsFragment"; 
        this.textAreaFragmentId = "idTextAreaFragment";               
        filterFragmentId = this.getView().createId(this.filterFragmentId);
        textAreaFragmentId = this.getView().createId(this.textAreaFragmentId);          
        this.filterDialog = this.byId('filterCbsDialog');
        this.textAreaDialog = this.byId('idTextAreaDialog');

        if (this.sPageName === "master") {
        	
        	this.oSearchDialog = this.byId('searchDialogMaterial');
        	fragmentId = this.getView().createId(this.searchFragmentId);
        	
        }
        	 this.getStatus();
            
            
        	 this.bindOnayList();
        	 this.BankaList();
        	     
    },   
    
    getStatus: function(){
    	var t = this;
    	 this.oView.getModel().callFunction("/GetStatus", {
             success: function(oData, Response) {
             	t.setPageStatus(oData.Status);
             	t.status = oData.Status;
               
             },
             error: function(error) { 
         
                 sap.m.MessageBox.alert(t.oResourceBundle.getText("PAGE_URL_DATA_GET_ERROR"));
             },
         });
    	
    },
    
    
    bindOnayList: function(){
    	var t = this;
   	 this.oView.getModel().read("/OnayListSet", null, null, true,
	            function(oData, oResponse) {
	       
	              var oModel = new sap.ui.model.json.JSONModel({
	                    EquiCollection: []
	              
	                });
	              
	              	oModel.setProperty("/OnayCollection", oData.results);
	                t.oView.setModel(oModel, "OnayModel");
	              
	            },
	            function(err) {
	               
	                sap.m.MessageBox.alert(t.oResourceBundle.getText("PAGE_DATA_GET_ERROR"));
	            });

   	this.oView.setModel(this.oComponent.getModel());
    	
    },

    handleNavButtonPress: function() {
        cus.ESAN.SOGirisOnay.utils.Util.onNavBack(-1);
    },
 
    onExit: function() {
 
    },
    setPageStatus: function (value) {
    	var t = this;
    	if(value == "Double"){
    		t.oSearchDialog.open();
    		 
    	}else if(value == "Yönetici"){
    		
    		t.byId("idTable").setVisible(true);
    		t.byId("idTable2").setVisible(false);
    	}else if (value == "Finans") {
    		
    		t.byId("idTable").setVisible(false);
    		t.byId("idTable2").setVisible(true);
		
    	}	
    },
    
    handleStatuSearchButtonPress: function(){
    	var t = this;
    	 fragmentId = this.getView().createId(this.searchFragmentId);
         var oStatu = sap.ui.core.Fragment.byId(fragmentId, "idStatusInput");
    	
    	if (oStatu.getSelectedKey() == "1"){
    		t.byId("idTable").setVisible(true);
    		t.byId("idTable2").setVisible(false);
    		t.oSearchDialog.close();
    	}else{
    		
    		t.byId("idTable").setVisible(false);
    		t.byId("idTable2").setVisible(true);
    		t.oSearchDialog.close();
    	}
    },
    
    BankaList: function(){
    	var t = this;
    	this.oView.getModel().read("/BankaListSet", null, null, true,
	            function(oData, oResponse) {
	       
	              var oModel = new sap.ui.model.json.JSONModel({
	            	  OdeyenBankaCollection: []
	              
	                });
	              
	              	oModel.setProperty("/OdeyenBankaCollection", oData.results);
	                t.oView.setModel(oModel, "OdeyenBankaModel");
	            },
	            function(err) {
	               
	                sap.m.MessageBox.alert(t.oResourceBundle.getText("PAGE_DATA_GET_ERROR"));
	            });
    	
    this.oView.setModel(this.oComponent.getModel());    
    
    },
    
  /*  _getFilter: function() { 
        var filters = [];
        var filterCount = 0; 
        
        // Nesne Türü...
		// -----------------------------------------------------------------//
        var oEartx = sap.ui.core.Fragment.byId(filterFragmentId, "idEartx");
        var eartxList = oEartx.getSelectedKeys();
        if (eartxList.length > 0) {
            for (var i = 0; i < eartxList.length; i++) {
                var oFilter = new sap.ui.model.Filter("Eqart", sap.ui.model.FilterOperator.EQ, eartxList[i]);
                filters.push(oFilter);
            }
            filterCount++;
        } 
        // Bölge...
		// ---------------------------------------------------------------//
        var oBolge = sap.ui.core.Fragment.byId(filterFragmentId, "idBolge");
        var bolgeList = oBolge.getSelectedKeys();
        if (bolgeList.length > 0) {
            for (var i = 0; i < bolgeList.length; i++) {
                var oFilter = new sap.ui.model.Filter("Werks", sap.ui.model.FilterOperator.EQ, bolgeList[i]);
                filters.push(oFilter);
            }
            filterCount++;
        } 
        // Yer...
		// -----------------------------------------------------------------//
        var oYer = sap.ui.core.Fragment.byId(filterFragmentId, "idYer");
        var yerList = oYer.getSelectedKeys();
        if (yerList.length > 0) {
            for (var i = 0; i < yerList.length; i++) {
                var oFilter = new sap.ui.model.Filter("Stort", sap.ui.model.FilterOperator.EQ, yerList[i]);
                filters.push(oFilter);
            }
            filterCount++;
        } 

        if (filterCount > 0) {
            this._addBadge("idFilter", "", filterCount.toString());
        } else {
            this._removeBadge("idFilter");
        }


        return new sap.ui.model.Filter({
            filters: filters,
            and: true
        });
    },
    handleOpenFilter: function() {
        this.byId("filterCbsDialog").open();
        this.hideAndShowDialogItems();
    },   
    handleCloseFilter: function() {
        this.byId("filterCbsDialog").close();
    },*/
    
    handleOpenTextArea: function(){
    	var t = this;
    	if(t.status=="Yönetici"){
    		
    		var sRow = this.byId("idTable").getSelectedIndex();
    	}else if (t.status=="Finans"|| t.status=="Double"){
    		
    		var sRow = this.byId("idTable2").getSelectedIndex();
    	}
    	
    	if(sRow >= 0){ 
        	this.byId("idTextAreaDialog").open();
    	}else{ 
        	sap.m.MessageBox.alert(t.oResourceBundle.getText("SELECT_ERROR"));
    	}
    },
    
    handleCloseTextArea: function(){ 
        var t = this; 
        var oText = sap.ui.core.Fragment.byId(textAreaFragmentId, "idTextArea"); 
  
        oText.setValue(""); 
		this.byId("idTextAreaDialog").close(); 
    }, 
    
    handleSearchButtonPress: function() {
        this.bindCbsConfirmList();
    }, 
    
    handleCloseSearchButtonP: function(){
    	this.filterDialog.close();
    }, 
    
    // ------------------------------- ONAY
	// ----------------------------------------------//
    handleSave: function(oEvent){
    	var t = this;
    	 fragmentId = this.getView().createId(this.searchFragmentId);
         var oStatu = sap.ui.core.Fragment.byId(fragmentId, "idStatusInput");
    	
    	if(t.status=="Yönetici"){
    		var sRows = this.byId("idTable")._oSelection.aSelectedIndices;
    	}else if (t.status=="Finans"){
    		var sRows = this.byId("idTable2")._oSelection.aSelectedIndices;
    	}else if (t.status=="Double"){
    		if(oStatu.getSelectedKey() == "1"){
    			var sRows = this.byId("idTable")._oSelection.aSelectedIndices;
    			t.status = "Yönetici"
    		}else{
    			var sRows = this.byId("idTable2")._oSelection.aSelectedIndices;
    			t.status = "Finans"
    		}
    		
    	}
   	 	
    	var sData = this.oView.getModel("OnayModel").getProperty("/OnayCollection");
    	var sBankData = this.oView.getModel("OdeyenBankaModel").getProperty("/OdeyenBankaCollection");
    	var oText = sap.ui.core.Fragment.byId(textAreaFragmentId, "idTextArea"); 
    	var aciklama = oText.getValue();
    	var count = 0;
    	for(i=0;i<sRows.length;i++){
    		var odemeno = sData[sRows[i]].Odemeno;
			if(t.status=="Yönetici"){
			t.acceptPayment(odemeno,aciklama);
		
	}else if (t.status =="Finans"){
		var onaylanantutar =sData[sRows[i]].Onaylanantutar;
		var taleptutari = sData[sRows[i]].Taleptutari;
		var odemetipi = sData[sRows[i]].Odemetipi;
		var odeyenbanka= t.byId("idTable2").mAggregations.rows[sRows[i]].mAggregations.cells[11].mProperties.selectedKey;
		var iban = sData[sRows[i]].Iban;
		var waers = sData[sRows[i]].Waers;
		var satici = sData[sRows[i]].Satici;
		var bankatipi = sData[sRows[i]].Bnktype;
		
		if(taleptutari > onaylanantutar || onaylanantutar === 0.000){
			sap.m.MessageBox.alert(t.oResourceBundle.getText("Onaylanan Tutar Talep Tutarından küçük veya eşit olmalıdır !"));
			return;
				}
    	
			else{
				 t.acceptPaymentFinance(odemeno,odemetipi,odeyenbanka,iban,aciklama,waers,onaylanantutar,satici,bankatipi,taleptutari);
				}
			
	}
	
	
}
    	//sap.m.MessageToast.show(count +" Ödeme Onaylandı");
    	
    	t.byId("idTextAreaDialog").close();
    },


    handleRedConfirm: function(){

    	var t = this;
    	
    	if(t.status=="Yönetici"){
    		var sRows = this.byId("idTable")._oSelection.aSelectedIndices;
    	}else if (t.status=="Finans"||t.status=="Double"){
    		var sRows = this.byId("idTable2")._oSelection.aSelectedIndices;
    	}
   	 
    	var sData = this.oView.getModel("OnayModel").getProperty("/OnayCollection");
    	var oText = sap.ui.core.Fragment.byId(textAreaFragmentId, "idTextArea"); 
    	var aciklama = oText.getValue();
    	var count =0 ;
    	for(i=0;i<sRows.length;i++){
    		var odemeno = sData[sRows[i]].Odemeno;
    				if(t.status=="Yönetici"){
    				t.rejectPayment(odemeno,aciklama);
    		}else if (t.status =="Finans"||t.status=="Double"){
    				var odemetipi = sData[sRows[i]].Odemetipi;
    				var odeyenbanka=sData[sRows[i]].Odeyenbanka;
    				var iban = sData[sRows[i]].Iban;
    				var waers = sData[sRows[i]].Waers;
    				var onaylanantutar =sData[sRows[i]].Onaylanantutar;
    				var taleptutari =sData[sRows[i]].Taleptutari;
    				var satici = sData[sRows[i]].Satici;
    				var bankatipi = sData[sRows[i]].Bnktype;
    			    t.rejectPaymentFinance(odemeno,odemetipi,odeyenbanka,iban,aciklama,waers,onaylanantutar,taleptutari,satici,bankatipi);		
    		}
    		
    	//	count++
    	}
    	
    //	sap.m.MessageToast.show(count +" Ödeme Reddedildi");
    	t.byId("idTextAreaDialog").close();
    }, 


    _showBusy: function() {
        this.busyCount += 1;
        sap.ui.core.BusyIndicator.show();
    },

    _hideBusy: function() {
        this.busyCount -= 1;
        if (this.busyCount <= 0) {
            this.busyCount = 0;
            sap.ui.core.BusyIndicator.hide();
        }
    },
    checkValue:function (e){
    	var t = this;
    	var sData = this.oView.getModel("OnayModel").getProperty("/OnayCollection");
    		var talep = sData[e.mParameters.id[e.mParameters.id.length-1]].Taleptutari;
        	var onay  = parseFloat(sData[e.mParameters.id[e.mParameters.id.length-1]].Onaylanantutar);
        	
        	if(onay>talep){
        		
        		t.byId("idOnaylanantutar").setValue("0.00");
        		sap.m.MessageBox.alert(t.oResourceBundle.getText("Onaylanan Tutar Talep Tutarından küçük veya eşit olmalıdır !"));
        	}
        	
    	
    
    	
    },
    rejectPayment: function (odemeno,aciklama){
    	
    	var t = this;
    	this.oView.getModel().callFunction("/RejectPayment", {
            urlParameters: {
                Odemeno: odemeno,
                Aciklama: aciklama,
                Type: "2"
                
            },
            success: function(oData, Response) {
            	t.bindOnayList();
            	sap.m.MessageToast.show(oData.Message);
            },
            error: function(error) {
         
                sap.m.MessageBox.alert(t.oResourceBundle.getText("PAGE_DYNAMIC_DATA_GET_ERROR"));
            }
		}); 
    	
    },
 rejectPaymentFinance: function (odemeno,odemetipi,odeyenbanka,iban,aciklama,waers,onaylanantutar,taleptutari,satici,bankatipi){
    	
    	var t = this;
    	this.oView.getModel().callFunction("/RejectPaymentFinance", {
            urlParameters: {
                Odemeno: odemeno,
                Odemetipi: odemetipi,
                Odeyenbanka :odeyenbanka,
                Onaylanantutar: onaylanantutar,
                Taleptutari : taleptutari,
                Satici : satici,
                Iban : iban ,
                Waers :waers,
                Aciklama : aciklama,
                Islmacik : aciklama,
                Bnktype: bankatipi,
                Type: "2"
                
            },
            success: function(oData, Response) {
            	t.bindOnayList();
            	sap.m.MessageToast.show(oData.Message);
            },
            error: function(error) {
         
                sap.m.MessageBox.alert(t.oResourceBundle.getText("PAGE_DYNAMIC_DATA_GET_ERROR"));
            }
		}); 
    	
    },
 acceptPaymentFinance: function (odemeno,odemetipi,odeyenbanka,iban,aciklama,waers,onaylanantutar,satici,bankatipi,taleptutari){
    	
    	var t = this;
    	this.oView.getModel().callFunction("/RejectPaymentFinance", {
            urlParameters: {
                Odemeno: odemeno,
                Odemetipi: odemetipi,
                Odeyenbanka :odeyenbanka,
                Onaylanantutar: onaylanantutar,
                Satici : satici,
                Iban : iban ,
                Waers :waers,
                Aciklama : aciklama,
                Islmacik : aciklama,
                Taleptutari: taleptutari,
                Bnktype:"EUR1",
                Type: "1"
             
                
            },
            success: function(oData, Response) {
            	t.bindOnayList();
            	sap.m.MessageToast.show(oData.Message);
            },
            error: function(error) {
            	
                sap.m.MessageBox.alert(t.oResourceBundle.getText("PAGE_DYNAMIC_DATA_GET_ERROR"));
            }
		}); 
    	
    },
    acceptPayment: function (odemeno,aciklama){	
    	var t = this;
    	this.oView.getModel().callFunction("/RejectPayment", {
            urlParameters: {
                Odemeno: odemeno,
                Aciklama: "",
                Type: "1"               
            },
            success: function(oData, Response) {
            	t.bindOnayList();
            	sap.m.MessageToast.show(oData.message);
            },
            error: function(error) {         
                sap.m.MessageBox.alert(t.oResourceBundle.getText("PAGE_DYNAMIC_DATA_GET_ERROR"));
            }
		}); 
    	
    },
    
    handleClose: function() {
        this.oSearchDialog = this.byId('searchDialogMaterial');
        this.oSearchDialog.close();
    },


});