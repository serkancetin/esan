jQuery.sap.declare("cus.ESAN.SOGirisOnay.Component");
jQuery.sap.require("sap.ui.core.UIComponent");
jQuery.sap.require("sap.ui.core.routing.History");
jQuery.sap.require("sap.m.routing.RouteMatchedHandler");
jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("cus.ESAN.SOGirisOnay.utils.Util");
jQuery.sap.require("cus.ESAN.SOGirisOnay.utils.messages");
jQuery.sap.require("cus.ESAN.SOGirisOnay.utils.formatter");

sap.ui.core.UIComponent.extend("cus.ESAN.SOGirisOnay.Component", {
    metadata: {
        "name": "Fio Equip Detail",
        "version": "1.1.0-SNAPSHOT",
        "library": "cus.ESAN.SOGirisOnay",
        "includes": ["css/fullScreenStyles.css"],
        "dependencies": {
            "libs": ["sap.m", "sap.ui.layout"],
            "components": [],
        },
        config: {
            resourceBundle: "i18n/i18n.properties",
            serviceConfig: {
                name: "OData",
                serviceUrl: "proxy/http/esanfiodev.esan.eczacibasi.com.tr:8000/sap/opu/odata/sap/YFIO_SO_003_SRV/"
            }
        },
        rootView: "cus.ESAN.SOGirisOnay.view.App",
        routing: {
            config: {
                viewType: "XML",
                viewPath: "cus.ESAN.SOGirisOnay.view",
                targetAggregation: "pages",
                targetControl: "fioriContent",
                clearTarget: false,
            },
            routes: [{
                pattern: "",
                name: "master",
                view: "Master", 
            }]
        }
    },

    init: function() {
        sap.ui.core.UIComponent.prototype.init.apply(this, arguments);

        this.i18nModelName = "i18n";
        this.viewElementModelName = "viewElementModel";
        this.deviceModelName = "device";
        var Config = this.getMetadata().getConfig();
        var sRootPath = jQuery.sap.getModulePath("cus.ESAN.SOGirisOnay");

        var oServiceConfig = Config.serviceConfig;
        var sServiceUrl = oServiceConfig.serviceUrl;

        this._routeMatchedHandler = new sap.m.routing.RouteMatchedHandler(this.getRouter(), this._bRouterCloseDialogs);

        this._initODataModel(sServiceUrl);

        var i18nModel = new sap.ui.model.resource.ResourceModel({
            bundleUrl: [sRootPath, Config.resourceBundle].join("/")
        });
        this.setModel(i18nModel, this.i18nModelName);

        cus.ESAN.SOGirisOnay.utils.Util._setResourceBundle(i18nModel.getResourceBundle());

        this.setModel(this._createViewElementModel(), this.viewElementModelName);
        this.setModel(this._createDeviceModel(), this.deviceModelName);

     
            sap.ui.getCore().getConfiguration().setLanguage("tr-TR");
            this.getRouter().initialize();
        
    },

    exit: function() {
        this._routeMatchedHandler.destroy();
    },

    setRouterSetCloseDialogs: function(bCloseDialogs) {
        this._bRouterCloseDialogs = bCloseDialogs;
        if (this._routeMatchedHandler) {
            this._routeMatchedHandler.setCloseDialogs(bCloseDialogs);
        }
    },

    _initODataModel: function(sServiceUrl) {
        var oConfig = {
            metadataUrlParams: {},
            json: true,
            defaultBindingMode: "OneWay",
            //defaultCountMode: "Inline",
            //useBatch: true,
            loadMetadataAsync: false
        };
        var oModel = new sap.ui.model.odata.ODataModel(sServiceUrl, oConfig);

        this.setModel(oModel);

    },

    _createViewElementModel: function() {
        var oViewElemProperties = {};
        if (sap.ui.Device.system.phone) {

        } else {

        }
        var oModel = new sap.ui.model.json.JSONModel(oViewElemProperties);
        oModel.setDefaultBindingMode("OneWay");
        return oModel;

    },

    _createDeviceModel: function() {
        var deviceModel = new sap.ui.model.json.JSONModel({
            isTouch: sap.ui.Device.support.touch,
            isNoTouch: !sap.ui.Device.support.touch,
            isPhone: sap.ui.Device.system.phone,
            isNoPhone: !sap.ui.Device.system.phone,
            listMode: sap.ui.Device.system.phone ? "None" : "SingleSelectMaster",
            listItemType: sap.ui.Device.system.phone ? "Active" : "Inactive",
            isTablet: sap.ui.Device.system.tablet || sap.ui.Device.resize.width < 950,
            isShowHideMode: sap.ui.Device.orientation.portrait || sap.ui.Device.resize.width < 950,
            isMobileDevice: sap.ui.Device.system.phone || sap.ui.Device.system.tablet
        });
        deviceModel.setDefaultBindingMode("OneWay");
        return deviceModel;
    }
});