jQuery.sap.declare("cus.ESAN.SOGirisOnay.utils.formatter");

cus.ESAN.SOGirisOnay.utils.formatter = {
    dateFormat: function(value) {
        if (!value) {
            return value;
        }

        var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({ pattern: "dd.MM.yyyy" });
        var dateFormatted = dateFormat.format(value);
        return dateFormatted;
    },
    
    dateFormatToOdata: function(c) {
        if (!value) {
            return value;
        }

        var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({ pattern: "yyyyMMdd" });
        var dateFormatted = dateFormat.format(value);
        return dateFormatted;
    },
    
    oDataTodateFormat: function(value){
    	if (!value) {
            return value;
        }
    	return value.substring(6, 8) + "." + value.substring(4, 6) + "." + value.substring(0, 4);
    },
    
    timeFormat: function(time) {
        if (time == undefined) return "";
        var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({ pattern: "HH:mm" });
        var TZOffsetMs = new Date(0).getTimezoneOffset() * 60 * 1000;
        var timeStr = timeFormat.format(new Date(time.ms + TZOffsetMs));
        return timeStr;
    },
    
    urlFormat: function(value) {
        return [cus.ESAN.SOGirisOnay.utils.Util.url, cus.ESAN.SOGirisOnay.utils.Util.Folder, value].join("/");
    },
    materialFormatToView: function(matnr,charg) {
        return matnr + "  -  " + charg;
    },
    materialFormatToView2: function(matnr) {
        return matnr + "  -  ";
    },

    materialFormatFromViewMatnr: function(value) {
//    	var matnr = value.split
//        return matnr + "  -  " + charg;
    },
    
    workOrderType: function(value) {
    	if(value === "ZAB"){
    		return "Arıza";
    	}else if(value === "ZKE"){
    		return "Kontrol";
    	}else if(value === "ZOB"){
    		return "Ölçüm";
    	}else if(value === "ZPB"){
    		return "Bakım";
    	}
    	return value;
    }
};