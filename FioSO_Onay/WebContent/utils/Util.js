jQuery.sap.declare("cus.ESAN.SOGirisOnay.utils.Util");

cus.ESAN.SOGirisOnay.utils.Util = {
    ApiKey: "AIzaSyCD2K89wGw41mu7AvQFI8bCkIrItFyF8pM",
    onNavBack: function(backPageCount, isLaunchpad) {
        var oHistory = sap.ui.core.routing.History.getInstance();
        var sPreviousHash = oHistory.getPreviousHash();
        if (sap.ushell) {
            var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
            if (isLaunchpad == undefined) {
                history.go(backPageCount);
            } else {
                //history.go(-1);
                oCrossAppNavigator.toExternal({
                    target: {
                        shellHash: "#"
                    }
                });
            }
        } else {
            history.go(backPageCount);
        }
    },

    isEmpty: function(value) {
        return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
    },

    _setResourceBundle: function(bundle) {
        this.oResourceBundle = bundle;
    },

    _getResourceBundle: function() {
        return this.oResourceBundle;
    },

    _generateTemplateUsingMetadata: function(_method, oDataModel) {
        var e = oDataModel.oMetadata._getEntityTypeByPath(_method);
        var t = {};
        for (var i in e.property) {
            if (e.property.hasOwnProperty(i)) {
                switch (e.property[i].type) {
                    case "Edm.Boolean":
                        {
                            t[e.property[i].name] = false;
                            break;
                        }
                    case "Edm.DateTime":
                        {
                            t[e.property[i].name] = null;
                            break;
                        }
                    case "Edm.Time":
                        {
                            t[e.property[i].name] = "PT00H00M00S";
                            break;
                        }
                    case "Edm.Int16":
                        {
                            t[e.property[i].name] = 0;
                            break;
                        }
                    case "Edm.Decimal":
                        {
                            t[e.property[i].name] = "0";
                            break;
                        }
                    default:
                        t[e.property[i].name] = "";
                        break;
                }
            }
        }
        return t;
    },

    _fillNewObject: function(sourceObject, targetObject) {
        var t = this;
        var changesExist = false;
        for (var key in targetObject) {
            if (targetObject.hasOwnProperty(key)) {
                var index = t.findInObject(sourceObject,
                    function(value) {
                        if (value == undefined) {
                            return undefined;
                        } else {
                            if (sourceObject[key] === undefined) return targetObject[key];
                            else return targetObject[key] = sourceObject[key];
                            changesExist = true;
                        }
                    });
            }
        }
        return changesExist;
    },

    findInObject: function(_object, _callBack) {
        for (var _filed in _object) {
            if (_object.hasOwnProperty(_filed)) {
                if (_callBack(_object[_filed])) {
                    return _filed;
                }
            }
        }
        return undefined;
    },

    findInArray: function(_array, _callBack) {
        for (var i = 0, len = _array.length; i < len; ++i) {
            if (_callBack(_array[i])) {
                return i;
            }
        }
        return -1;
    },

    checkResponseHeader: function(header) {
        var returnData = false;
        var result = header["sap-message"];
        if (result) {
            var json = JSON.stringify(eval("(" + result + ")"));
            var obj = JSON.parse(json);
            if (obj.severity === "error") {
                var message = obj.message;
                sap.m.MessageBox.show(message,
                    sap.m.MessageBox.Icon.ERROR,
                    cus.ESAN.SOGirisOnay.utils.Util._getResourceBundle().getText("ERROR"), [sap.m.MessageBox.Action.OK]
                );
            } else if (obj.severity === "info") {
                var message = obj.message;
                sap.m.MessageBox.show(message,
                    sap.m.MessageBox.Icon.INFO,
                    cus.ESAN.SOGirisOnay.utils.Util._getResourceBundle().getText("SUCCESS"), [sap.m.MessageBox.Action.OK]
                );
                // returnData = true;
            }

            return returnData;
        }
        return false;
    },

    sendBatchReadRequests: function(m, r, c, a) {
        var b = [];
        for (var i in r) {
            var R = m.createBatchOperation(r[i], "GET");
            b.push(R);
        }
        cus.ESAN.SOGirisOnay.utils.Util.sendBatchReadOperations(m, b, c, a);
    },

    sendBatchReadOperations: function(m, b, c, a) {
        m.clearBatch();
        m.addBatchReadOperations(b);
        m
            .submitBatch(
                function(d) {
                    var e = null;
                    var B = d.__batchResponses;
                    var r = {};
                    for (var i = 0; i < B.length; i++) {
                        if (B[i].statusCode === "200") {
                            var k = b[i].requestUri;
                            if (B[i].data.results) {
                                r[k] = B[i].data.results;
                            } else if (B[i].data.__metadata) {
                                if (!r[k]) {
                                    r[k] = B[i].data;
                                } else if (r[k] instanceof Array) {
                                    r[k].push(B[i].data);
                                } else {
                                    var o = r[k];
                                    r[k] = [o];
                                    r[k].push(B[i].data);
                                }
                            } else
                                r[k] = B[i].data[Object.keys(B[i].data)[0]];
                        } else {
                            var R = jQuery
                                .parseJSON(B[i].response.body);
                            e = {
                                type: sap.ca.ui.message.Type.ERROR,
                                code: R.error.code,
                                message: R.error.message.value,
                                details: R.error.innererror.Error_Resolution.SAP_Note
                            };
                        }
                    }
                    if (e) {
                        if (a)
                            a(e);
                        else
                            sap.ca.ui.message.showMessageBox(e);
                    } else {
                        c(r);
                    }
                },
                function(e) {
                    if (a) {
                        e.response.body = jQuery
                            .parseJSON(e.response.body);
                        e = {
                            type: sap.ca.ui.message.Type.ERROR,
                            code: e.response.body.error.code,
                            message: e.response.body.error.message.value,
                            details: e.response.body.error.innererror.Error_Resolution.SAP_Note
                        };
                        a(e);
                    } else {
                        jQuery.sap.log.error("Read failed in Util.js->sendBatchReadOperations");
                    }
                }, true);
    },

    sendBatchChangeOperations: function(m, b, c, a) {
        m.clearBatch();
        m.addBatchChangeOperations(b);
        var t = this;
        m.submitBatch(jQuery.proxy(function(d) {
            var e = {};
            var B = d.__batchResponses;
            var r = [];
            for (var i = 0; i < B.length; i++) {
                if (B[i].response) {
                    e = jQuery.parseJSON(B[i].response.body).error;
                    e.statusCode = B[i].response.statusCode;
                    e.statusText = B[i].response.statusText;
                    cus.ESAN.SOGirisOnay.utils.Util.checkResponseHeader(B[i].response);
                }
                if (B[i].__changeResponses && B[i].__changeResponses.length > 0 && B[i].__changeResponses[0].statusCode === "201") {
                    r.push(B[i].__changeResponses[0].data);
                    cus.ESAN.SOGirisOnay.utils.Util.checkResponseHeader(B[i].__changeResponses[0].headers);
                }
            }
            if (!e.message) {
                if (c)
                    c.call(t, r);
            } else {
                if (a)
                    a.call(t, e);
                else
                    sap.m.MessageBox.alert(e.message.value);
            }
        }, this), function(error) {
            sap.ui.core.BusyIndicator.hide();
            sap.m.MessageBox.alert("SYSTEM_ERROR");
        }, true);
    },
    //Login işlemleri BEGIN

    
};