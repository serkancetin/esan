jQuery.sap.declare("cus.ESAN.FioSOGiris.utils.formatter");

cus.ESAN.FioSOGiris.utils.formatter = {

		currencyValue: function(sValue) {
			if (!sValue) {
				return "";
			}

			return parseFloat(sValue).toFixed(2);
		}
};

