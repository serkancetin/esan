jQuery.sap.declare("cus.ESAN.FioSOGiris.utils.messages");
jQuery.sap.require("sap.ca.ui.message.message");

cus.ESAN.FioSOGiris.utils.messages = {};

cus.ESAN.FioSOGiris.utils.messages.showErrorMessage = function(oParameter) {
    var oErrorDetails = cus.ESAN.FioSOGiris.utils.messages._parseError(oParameter);
    var oMsgBox = sap.ca.ui.message.showMessageBox({
        type: sap.ca.ui.message.Type.ERROR,
        message: oErrorDetails.sMessage,
        details: oErrorDetails.sDetails
    });
    if (!sap.ui.Device.support.touch) {
        oMsgBox.addStyleClass("sapUiSizeCompact");
    }
};

cus.ESAN.FioSOGiris.utils.messages.getErrorContent = function(oParameter) {
    return cus.ESAN.FioSOGiris.utils.messages._parseError(oParameter).sMessage;
};

cus.ESAN.FioSOGiris.utils.messages._parseError = function(oParameter) {
    var sMessage = "",
        sDetails = "",
        oEvent = null,
        oResponse = null,
        oError = {};

    if (oParameter.mParameters) {
        oEvent = oParameter;
        sMessage = oEvent.getParameter("message");
        sDetails = oEvent.getParameter("responseText");
    } else {
        oResponse = oParameter;
        sMessage = oResponse.message;
        sDetails = oResponse.response.body;
    }

    if (jQuery.sap.startsWith(sDetails, "{\"error\":")) {
        var oErrModel = new sap.ui.model.json.JSONModel();
        oErrModel.setJSON(sDetails);
        sMessage = oErrModel.getProperty("/error/message/value");
    }

    oError.sDetails = sDetails;
    oError.sMessage = sMessage;
    return oError;
};