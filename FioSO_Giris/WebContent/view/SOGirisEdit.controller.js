sap.ui.controller("cus.ESAN.FioSOGiris.view.SOGirisEdit", {
    onInit: function() {
        this.oView = this.getView();
        this.oComponent = sap.ui.component(sap.ui.core.Component.getOwnerIdFor(this.oView));
        this.oResourceBundle = this.oComponent.getModel(this.oComponent.i18nModelName).getResourceBundle();
        this.oRouter = this.oComponent.getRouter();
        this.oRouter.attachRoutePatternMatched(this.onRoutePatternMatched, this);
    },
    
    onRoutePatternMatched: function(oEvent) {
		// Register the view with the message manager
		this.bvtyp;
		sap.ui.getCore().getMessageManager().registerObject(this.getView(), true);
		var oMessagesModel = sap.ui.getCore().getMessageManager().getMessageModel();
		this._oBinding = new sap.ui.model.Binding(oMessagesModel, "/", oMessagesModel.getContext("/"));
		this._oBinding.attachChange(function(oEvent) {
			var aMessages = oEvent.getSource().getModel().getData();
			for (var i = 0; i < aMessages.length; i++) {
				if (aMessages[i].type === "Error" && !aMessages[i].technical) {
					that._oViewModel.setProperty("/enableCreate", false);
				}
			}
		});      	
        this.sPageName = oEvent.getParameter("name");
  		this._oViewModel;
		this.shsaticibanka  = [];   		
        var oArguments = oEvent.getParameter("arguments");

        if (this.sPageName === "SOGirisEdit") {
        	 this.sOdemeno = oArguments.Odemeno;
     		this.shwaers  = []; 
            this.getEventBus().publish("SOGiris", "SelectElementOtherPage", this.sOdemeno);
            this.setPageData(); 
        }
    },
    
    //------------------------------- Busy Indicator ----------------------------------------------//
    _showBusy: function() {
        this.getEventBus().publish("SOGiris", "ShowBusy", null);
    },

    _hideBusy: function() {
        this.getEventBus().publish("SOGiris", "HideBusy", null);
    },
    //------------------------------- Busy Indicator ----------------------------------------------//
 

    handleNavButtonPress: function() {
        cus.ESAN.FioSOGiris.utils.Util.onNavBack(-1);
    },
    
    

    getEventBus: function() {
        return sap.ui.getCore().getEventBus();
    },
    
    onAfterRendering:function(){
		//this._getList();
		//this._getListP();
	},

 
    setPageData: function() {
        var t = this;
    	t._showBusy();
//        var sObjectPath = this.oView.getModel().createKey("/LoginSet", {
//            Guid: this.oComponent.UserInfo.Guid
//        });
        var sOlcumPath = this.oView.getModel().createKey("/SaticiOdemeGirisSet", {
            Odemeno: this.sOdemeno,
//            Sernr: ""
        });
//        this.MeasPointSil = this.byId("idMeasPointSil");
        this.oView.getModel().read( sOlcumPath , null, null, true,
        		function(oData, oResponse) {
            var oModel = new sap.ui.model.json.JSONModel({
            	SOGirisCollection: []
            });
            oModel.setProperty("/SOGirisCollection", oData);
            t.oView.setModel(oModel, "SOGirisModel");
            t.oView.bindElement({
                path: "/SOGirisCollection",
                model: "SOGirisModel"
            });
            t._hideBusy();
        },
        function(err) {
            t._hideBusy();
            sap.m.MessageBox.alert(t.oResourceBundle.getText("PAGE_DATA_GET_ERROR"));
        });

			// Parabirimi Listesi Cekelim
		this._showBusy();
        var sPath = "/ShWaersSet";
        this.oView.getModel().read(sPath, null, null, true,
            function (oData, oResponse) {
                var oModel = new sap.ui.model.json.JSONModel({
                	SOGirisWaersCollection: []
                });

                t.shwaers = oData.results;
                oModel.setProperty("/SOGirisWaersCollection", t.shwaers);
                t.oView.setModel(oModel, "SOGirisWaersModel");

                t._hideBusy();
            }, function (err) {
            	t._hideBusy();
            });   	        

},
    

    

//<-- > Begin Of  Parabirimi ve tanımlarının secimi için yazıldı
          
        onValueHelpWaersRequest: function() {
            this.handleWaersOpenDialog();
        },

        handleWaersOpenDialog: function() {
            var t = this;

            if (!this._oDialogWaers) {
                this._oDialogWaers = sap.ui.xmlfragment("cus.ESAN.FioSOGiris.view.fragment.WaersDialog", this);
            }

            oModel = new sap.ui.model.json.JSONModel();
            oModel.setProperty("/SOGirisWaersCollection", this.shwaers);
            this._oDialogWaers.setModel(oModel, "SOGirisWaersModel");
            this._oDialogWaers.open();             
        },
        
        pressWaersSearch: function() {
            this._waersSearch("idWaers", "idLtext");
        },
        
        _waersSearch: function(helpInput, ltext) {

            var t = this;
            var searchinput = t.byId(helpInput).getValue();
            var sPath = "ShWaersSet('" + searchinput + "')";

            this.oView.getModel().read(sPath, null, null, true,
                function(oData, oResponse) {
                    t.byId(helpInput).setValue(oData.Waers);
                    t.byId(ltext).setValue(oData.Ltext);
                },
                function(error) {
                    sap.m.MessageToast.show(getText("Para Birimleri Bulunamadı"));
                });
        },

        //*** Filtrenin calısması için yazıldı.
        filterWaersListLive: function(oEvent) {
        	var value = oEvent.getParameter("value");
            var oF1 = new sap.ui.model.Filter("Waers", sap.ui.model.FilterOperator.Contains, value);
            var oF2 = new sap.ui.model.Filter("Ltext", sap.ui.model.FilterOperator.Contains, value);
            var oFilters = new sap.ui.model.Filter({
                filters: [
                    oF1,
                    oF2,
                ],
                and: false
            });
            oEvent.getSource().getBinding("items").filter(oFilters, sap.ui.model.FilterType.Application);
          },  
          
          handleConfirmWaers: function(oEvent) {
              if(oEvent.sId === "confirm"){   
                  var waers = this.byId("idWaersInput").setValue(oEvent.mParameters.selectedItem.getCells()[0].mProperties.title); 
                  this.getbakiyedetay();  
                  this.getibandetay();                        
                  }
             // this.byId("idWaersInput").setEnabled(false);
              this.byId("idIbanInput").setEnabled(true);
          } ,   

      getbakiyedetay: function() {
			this._showBusy();
			var t = this;
			this.oView.getModel().callFunction("/BakiyeDetayFunc", {
				urlParameters: {
					Satici: t.byId("idSaticiNoInput").getValue(),
					Waers: t.byId("idWaersInput").getValue()
        },
        success: function(oData, Response) {
        	t.byId("idBakiye1Input").setValue(oData.Bakiye1);
        	t.byId("idBakiye2Input").setValue(oData.Bakiye2);
        	t.byId("idBakiye3Input").setValue(oData.Bakiye3);            	
       	  	t._hideBusy();
        },
        error: function(error) {
            t._hideBusy();
        },
			}); 
          },
                      
        	  
//  <-- > End Of          ************  ************   ************   ************   ************   ************ 

//  <-- > Begin Of  Satıcıya ait Banka ve Para Birimi secimi için yazıldı
          
          onValueHelpSaticiBankaRequest: function() {
              this.handleSaticiBankaOpenDialog();
          },

          handleSaticiBankaOpenDialog: function() {
              var t = this;

              if (!this._oDialogSaticiBanka) {
                  this._oDialogSaticiBanka = sap.ui.xmlfragment("cus.ESAN.FioSOGiris.view.fragment.SaticiBankaDialog", this);                    
              }

              oModel = new sap.ui.model.json.JSONModel();
              oModel.setProperty("/SOGirisSaticiBankaCollection", this.shsaticibanka);
              this._oDialogSaticiBanka.setModel(oModel, "SOGirisSaticiBankaModel");
              this._oDialogSaticiBanka.open();
          },
          
          pressSaticiBankaSearch: function() {
              this._saticibankaSearch("idIban", "idWaersIban","idBvtypIban");
          },

          _saticibankaSearch: function(helpInput, waers,bvtyp) {

              var t = this;
              var searchinput = t.byId(helpInput).getValue();
              var sPath = "ShSaticiBankaSet('" + searchinput + "')";

              this.oView.getModel().read(sPath, null, null, true,
                  function(oData, oResponse) {
                      t.byId(helpInput).setValue(oData.Iban);
                      t.byId(waers).setValue(oData.Waers);
                      t.byId(bvtyp).setValue(oData.Bvtyp);                      
                  },
                  function(error) {
                      sap.m.MessageToast.show(getText("IBAN bilgisi bulunamadı"));
                  });
          },
          
          //*** Filtrenin calısması için yazıldı.
          filterSaticiBankaListLive: function(oEvent) {
          	var value = oEvent.getParameter("value");
              var oF1 = new sap.ui.model.Filter("Iban", sap.ui.model.FilterOperator.Contains, value);
              var oF2 = new sap.ui.model.Filter("Waers", sap.ui.model.FilterOperator.Contains, value);
              var oF3 = new sap.ui.model.Filter("Bvtyp", sap.ui.model.FilterOperator.Contains, value);              
              var oFilters = new sap.ui.model.Filter({
                  filters: [
                      oF1,
                      oF2,
                      oF3,
                  ],
                  and: false
              });
              oEvent.getSource().getBinding("items").filter(oFilters, sap.ui.model.FilterType.Application);
            },  
            

            handleConfirmSaticiBanka: function(oEvent) {
          	  this.bvtyp = oEvent.mParameters.selectedItem.mAggregations.cells[2].mProperties.title;            	
                if(oEvent.sId === "confirm"){
                    
                    this.byId("idIbanInput").setValue(oEvent.mParameters.selectedItem.getCells()[0].mProperties.title);                    
                }
                this.byId("idIbanInput").setEnabled(false);
                this.byId("idTalepTutariInput").setEnabled(true);
                      
            },  

          getibandetay: function() {
			this._showBusy();
			var t = this;
			this.oView.getModel().callFunction("/GetIban", {
				urlParameters: {
					Satici: t.byId("idSaticiNoInput").getValue(),
					Waers: t.byId("idWaersInput").getValue()
          },
          success: function(oData, Response) {
              oModel = new sap.ui.model.json.JSONModel();
              oModel.setProperty("/SOGirisSaticiBankaCollection", oData.results);
              t.shsaticibanka = oData.results;
           	t._hideBusy();
          },
          error: function(error) {
              t._hideBusy();
          },
      }); 
            }, 
//<-- > End Of  Satıcıya ait Banka ve Para Birimi secimi için yazıldı
            
    //-----SicilNo Begin----//
    handleOpenDialogMaster: function() {
        
    },

    onValueHelpRequest: function() {
        this.handleOpenDialog();
    },
    
//    pressPersSearch: function() {
//        this._personelSearch("idSicilNo", "idName", "idSurname");
//    },
//    handleOpenDialog: function() {
//        var t = this;
////        
//        if (!this._oDialog) {
//			this._oDialog = sap.ui.xmlfragment("cus.ESAN.FioSOGiris.view.fragment.Dialog", this);
//		}
//        var oModel = new sap.ui.model.json.JSONModel({
//        	UserCollectionPrefix: []
//        });
////        t.persList = oData.results;
//        oModel.setProperty("/PersCollectionPrefix", this.persList);
//        this._oDialog.setModel(oModel, "PersGroupsModel");
//        //this._oDialog._setModel("PersGroupsModel>/PersCollectionPrefix");
////        this._oDialog._setBindingContext("PersCollectionPrefix");
//       // this.getView().addDependent(this._oDialog);
//        this._oDialog._dialog._oBeginButton.setType("Reject")
//        this._oDialog.open();
//    },


    handleConfirm: function() {
        //        this.oPersDlg.close();
        this.oPersDlg.setVisible(false);
    },

    
    getSplitAppObj: function() {
        var result = this.byId("SplitAppDemo");

        if (!result) {
            jQuery.sap.log.info("SplitApp object can't be found");
        }
        return result;
    },

	

//  <-- > Begin Of  Odeme talebini kayıt edelim
    
    handleSaveButton: function() {
  	  if (this.byId("idIbanInput").getValue() == "" ) {  sap.m.MessageToast.show("Satici Banka Bakımı Yapınız.") ; return;}   
  	  
  	  // Bakiyelere gore talep tutarı kontrolu yapılacak
  	  if (this.byId("idBakiye1Input").getValue() == "" && 
  	  	  this.byId("idBakiye2Input").getValue() == "" && 
  	  	  this.byId("idTalepTutariInput").getValue() !== "")  
  	  {  sap.m.MessageToast.show("Satıcıya ait bakiye bulunamadı.Talep girişi yapılamaz..") ; return;}
  	 
  	 var lv_bakiye_check; 
  	 var lv_bakiye1 =  this.byId("idBakiye1Input").getValue();
  	 var lv_taleptutari = this.byId("idTalepTutariInput").getValue();  
  	 var lv_bakiye3 = this.byId("idBakiye3Input").getValue();
  	 lv_bakiye_check = lv_bakiye1 - lv_bakiye3;
  	 if (lv_bakiye_check < lv_taleptutari)
    	  {  sap.m.MessageToast.show("Talep tutarı,Vadesi gelen bakiye ve eski taleplerin toplamından küçük olmalıdır,Lütfen Avans sayfasından giriş yapınız.") ; return;}
  		 
  	  
  	  // İşlem acıklaması icin konusulacak
//  	  if (this.byId("idIslemAciklamaInput").getValue() == "" )
//  	  { sap.m.MessageBox.alert("**Opsiyonel-İşlem Açıklama Girişi.") ; return;}
  	  
  	  this._showBusy();
        var t = this;
        var saveSet = {
//            Statu: "GIRIS",
//            Satici:  this.byId("idSaticiNoInput").getValue(),
//            Name1:   this.byId("idName1Input").getValue(),
//            Name2:   this.byId("idName2Input").getValue(),                       
            Waers:   this.byId("idWaersInput").getValue(), 
//            Stcd2:   this.byId("idStcd2Input").getValue(),             
            Bakiye1: this.byId("idBakiye1Input").getValue(),
            Bakiye2: this.byId("idBakiye2Input").getValue(),
            Bakiye3: this.byId("idBakiye3Input").getValue(),  
            Bakiye4: this.byId("idBakiye3Input").getValue(),             
            Iban:    this.byId("idIbanInput").getValue(),
            Taleptutari:   this.byId("idTalepTutariInput").getValue(),
            Islmacik: this.byId("idIslemAciklamaInput").getValue(),
            Odemeno: this.byId("idOdemenoInput").getValue(),
            //Odemetipi: "",
            //Waerk: "",
            //Odeyenbanka: "",
            //Odeyenbankaiban: "",
            //Yoneticinotu: "",
            //Finansnotu: "",
            Onaylanantutar: "0.00", 
            //Belnr: "",
            //Bukrs: "",
            //Gjahr: "",
            //Uname: "",
            //Datum:"",
            //Uzeit: "",
            //Zreturn:"",
            //Zmessage:"",
            Bvtyp: t.bvtyp,
            Odmbcm:"F",
            //Talepno:"",
            //Bankulke:"",
            //Bankanah:"",
            //Bankhesn:"",
        };
        var oModel = this.oView.getModel();
        var batch = oModel.createBatchOperation("/SaticiOdemeGirisSet", "POST", saveSet);
        var batchList = [batch]; 
        cus.ESAN.FioSOGiris.utils.Util.sendBatchChangeOperations(oModel, batchList, 
       function(s) {
            if (s[0].Zreturn === "SUCCESS") {
          	  t._hideBusy();
          	 // t.oResourceBundle.getText("I18TANIMI"),
          	  sap.m.MessageBox.alert(s[0].Zmessage);
          	  t.oRouter.navTo("SOGirisDetail", {
                       from: "SOGirisEdit",
                       Odemeno: t.byId("idOdemenoInput").getValue(),
                   });
            }},
       function(e) {
            t._hideBusy();
            sap.m.MessageBox.alert(e.message.value);
        });
   
    },
            
//<-- > End Of  Odeme talebini kayıt edelim              


//<-- > Begin Of Tutar alan kontrolu       
	_validateSaveEnablement: function() {
		var aInputControls = this._getFormFields(this.byId("newEntitySimpleForm"));
		var oControl;
		for (var m = 0; m < aInputControls.length; m++) {
			oControl = aInputControls[m].control;
			if (aInputControls[m].required) {
				var sValue = oControl.getValue();
				if (!sValue) {
					this._oViewModel.setProperty("/enableCreate", false);
					return;
				}
			}
		}
		this._checkForErrorMessages();
	},

	_getFormFields: function(oSimpleForm) {
		var aControls = []; var sInput = [];
		var aFormContent = oSimpleForm.getFormContainers()[0].getFormElements();
//		oSimpleForm.getFormContainers()[0].a.getFormElements()[9].getLabel().__sLabeledControl
		var sControlType;
		for (var i = 0; i < aFormContent.length; i++) {
			sControlType = this.byId(aFormContent[i].getLabel().__sLabeledControl).getMetadata().getName();
			
			sInput.push(this.byId(aFormContent[i].getLabel().__sLabeledControl));
			if (sControlType === "sap.m.Input" || sControlType === "sap.m.DateTimeInput" ||
				sControlType === "sap.m.CheckBox") {
				aControls.push({
					control: sInput[i],
					required: sInput[i].getRequired && sInput[i].getRequired()
				});
			}
		}
		return aControls;
	},
	
	_checkForErrorMessages: function() {
		var aMessages = this._oBinding.oModel.oData;
		if (aMessages.length > 0) {
			var bEnableCreate = true;
			for (var i = 0; i < aMessages.length; i++) {
				if (aMessages[i].type === "Error" && !aMessages[i].technical) {
					bEnableCreate = false;
					break;
				}
			}
			
			this._oViewModel.setProperty("/enableCreate", bEnableCreate);
		} else {
			this._oViewModel.setProperty("/enableCreate", true);
		}
	},   		
//<-- > End Of Tutar alan kontrolu 
 
});

    
    