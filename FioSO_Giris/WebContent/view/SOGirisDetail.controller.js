sap.ui.controller("cus.ESAN.FioSOGiris.view.SOGirisDetail", {
    onInit: function() {
        this.oView = this.getView();
        this.oComponent = sap.ui.component(sap.ui.core.Component.getOwnerIdFor(this.oView));
        this.oResourceBundle = this.oComponent.getModel(this.oComponent.i18nModelName).getResourceBundle();
        this.oRouter = this.oComponent.getRouter();
        this.mpt;
        this.oRouter.attachRoutePatternMatched(this.onRoutePatternMatched, this);
    },
    onRoutePatternMatched: function(oEvent) {
        this.sPageName = oEvent.getParameter("name");
        var oArguments = oEvent.getParameter("arguments");
       // this.mpt = oArguments.Odemeno;


        if (this.sPageName === "SOGirisDetail") {
            this.sOdemeno = oArguments.Odemeno;
            this.getEventBus().publish("SOGiris", "SelectElementOtherPage", this.sOdemeno);
            this.setPageData();
        }

        this.listenResize();
    },

    //------------------------------- Busy Indicator ----------------------------------------------//
    _showBusy: function() {
        this.getEventBus().publish("SOGiris", "ShowBusy", null);
    },

    _hideBusy: function() {
        this.getEventBus().publish("SOGiris", "HideBusy", null);
    },
    //------------------------------- Busy Indicator ----------------------------------------------//
    
//    MasDocMove:function (){
//    	   			var params = {
//    	   		    refPage: "ZFIOMPSEM",
//    	            Mpoint: this.mpt,
//    	            Equnr: this.byId("idEqunr").getText(),
//    	            Eqktx: this.byId("idEqktx").getText()
//    	        };
//    	        this.navToCrossApp("ZFIOMDSEM", params);
//    	
//    },
    
    navToCrossApp: function(sSemanticObject, sParams) {
        if (window.location.hostname == "localhost") {
            return;
        }
        var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
        var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
            target: {
                semanticObject: sSemanticObject,
                action: "display"
            },
            params: sParams
        })) || "";
        oCrossAppNavigator.toExternal({
            target: {
                shellHash: hash
            }
        });
    },
    handleIconTabBarSelect: function(oEvent) {
    	var sKey = oEvent.getParameter("key");
    	if (sKey === "Ok") {
        cus.ESAN.FioSOGiris.utils.Util.onNavBack(-1);
    	}
    },
    
    handleNavButtonPress: function(oEvent) {
    	
        this.getEventBus().publish("SOGiris", "ShowHideMaster", "show");
    	
    },

    listenResize: function() {
        var t = this;
        sap.ui.Device.resize.attachHandler(function(mParams) {
            var oModel = t.oComponent.getModel("device");
            if (mParams.width > 950 && !sap.ui.Device.orientation.portrait) {
                oModel.oData.isShowHideMode = false;
                t.byId("showMasterButton").setVisible(false);
            } else {
                oModel.oData.isShowHideMode = true;
                t.byId("showMasterButton").setVisible(true);
            }
        });
    },


    onPressNavToUptade: function() 
    	{
      	var Statu = this.byId("idStatu").getText();
    	if (Statu === "GIRIS")  
    
        this.oRouter.navTo("SOGirisEdit", {
            from: "SOGirisDetail",
            Odemeno: this.sOdemeno });
            else 
            {
            sap.m.MessageToast.show('Sadece GIRIS Statusundeki Talepler Degiştirilebilir.');	
            } 
        
    },

    setPageData: function() {
        var t = this;
    	this._showBusy();
//        var sObjectPath = this.oView.getModel().createKey("/LoginSet", {
//            Guid: this.oComponent.UserInfo.Guid
//        });
        var sOlcumPath = this.oView.getModel().createKey("/SaticiOdemeGirisSet", {
            Odemeno: this.sOdemeno,
//            Sernr: ""
        });
//        this.MeasPointSil = this.byId("idMeasPointSil");
        this.oView.getModel().read( sOlcumPath , null, null, true,
            function(oData, oResponse) {
                var oModel = new sap.ui.model.json.JSONModel({
                    SOGirisCollection: []
                });
                oModel.setProperty("/SOGirisCollection", oData);
                t.oView.setModel(oModel, "SOGirisModel");
                t.oView.bindElement({
                    path: "/SOGirisCollection",
                    model: "SOGirisModel"
                });
                t._hideBusy();
            },
            function(err) {
                t._hideBusy();
                sap.m.MessageBox.alert(t.oResourceBundle.getText("PAGE_DATA_GET_ERROR"));
            });
    },
    getEventBus: function() {
        return sap.ui.getCore().getEventBus();
    },

//    handleOpen: function(oEvent) {
//        var oButton = oEvent.getSource();
//
//        // create action sheet only once
//        if (!this._actionSheet) {
//            this._actionSheet = sap.ui.xmlfragment(
//                "cus.ESAN.FioSOGiris.view.fragment.ActionSheet",
//                this
//            );
//            this.getView().addDependent(this._actionSheet);
//        }
//
//        this._actionSheet.openBy(oButton);
//    },

//    onDelete: function() {
//        if (this.byId("idOdemeno").getText() === "") {
//            return;
//        }
//        this.MeasPointSil.setVisible(true);
//        this.MeasPointSil.open();
//    },

//    handleRejectMeasPointDelete: function() {
//        this.MeasPointSil.close();
//        this.MeasPointSil.setVisible(false);
//    },

//    navToEq:function(oEvent){
//    	
//    	 var source = oEvent.oSource.mProperties.text;
//         var params = {
//             Equnr: source,
//             MPoint : this.mpt,
//             refPage: "ZFIOMPSEM",
//         };
//         this.navToCrossApp("ZFIOEQUISEM", params);
//     
//    	
//    },
    navToCrossApp: function(sSemanticObject, sParams) {
        if (window.location.hostname == "localhost") {
            return;
        }
        var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
        var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
            target: {
                semanticObject: sSemanticObject,
                action: "display"
            },
            params: sParams
        })) || "";
        oCrossAppNavigator.toExternal({
            target: {
                shellHash: hash
            }
        });
    },
    
//    handleAcceptMeasPointDelete: function() {
//        var t = this;
//        var oModel = this.oView.getModel();
//    	this._showBusy();
//        var m = this.mpt;
//        var saveSet = {
//            Odemeno: this.byId("idOdemeno").getText(),
//            Guid: this.oComponent.UserInfo.Guid,
//            Odemeno: m,
//            Return: "",
//            Message: "",
//        }
//
//        var batch = oModel.createBatchOperation("/OlcumSilSet", "POST", saveSet);
//        var batchList = [batch];
//
//        cus.ESAN.FioSOGiris.utils.Util.sendBatchChangeOperations(oModel, batchList, function(s) {
//            if (s[0].Return === "SUCCESS") {
//                t._moveDeletedMeasPoint();
//                t.MeasPointSil.close();
//                t.MeasPointSil.setVisible(false);
//
//                t._hideBusy();
//                sap.m.MessageToast.show(t.oResourceBundle.getText(s[0].Message));
//            } else {
//                t._hideBusy();
//                sap.m.MessageToast.show(t.oResourceBundle.getText(s[0].Message));
//            }
//        }, function(e) {
//            t._hideBusy();
//            sap.m.MessageBox.alert(e.message.value);
//        });
//    },

//    _moveDeletedMeasPoint: function() {
//        this.getEventBus().publish("SOGiris", "DeleteMeasPoint", this.sOdemeno);
//    },
//    
//    onPressLog: function() { 
//    	this._readMeasPointLog();
//    },
//    
//    _readMeasPointLog: function(c) { 
//    	this._showBusy();
//        this.measPointFragment = "searchFragment";
//		fragmentId = this.getView().createId(this.measPointFragment);
//		sap.ui.core.Fragment.byId(fragmentId, "idMeasPointLog").setBusyIndicatorDelay(0);
//		sap.ui.core.Fragment.byId(fragmentId, "idMeasPointLog").setBusy(true); 
//		var t = this; 
//        this.oView.getModel().callFunction("/GetMeasPointLog", {
//            urlParameters: {
//                Guid: this.oComponent.UserInfo.Guid,
//                MeasPoint: this.mpt
//            },
//            success: function(oData, Response) {
//                oModel = new sap.ui.model.json.JSONModel(); 
//                oModel.setProperty("/MeasPointLogCollection", oData.results);
//                t.oView.setModel(oModel, "MeasPointLogModel");  
//        		sap.ui.core.Fragment.byId(fragmentId, "idMeasPointLog").setBusy(false);
//                t._hideBusy();
//            },
//            error: function(error) {  
//        		sap.ui.core.Fragment.byId(fragmentId, "idMeasPointLog").setBusy(false);
//                sap.m.MessageBox.alert(t.oResourceBundle.getText("PAGE_DYNAMIC_DATA_GET_ERROR"));
//                t._hideBusy();
//            },
//        });
//        
//        this.byId("measPointLog").open();  
//		
//	}, 
	
//	   handleLogClosePress: function() {  
//	        this.byId("measPointLog").close();
//	    },

});