sap.ui.controller("cus.ESAN.FioSOGiris.view.SOGirisCreate", {
    onInit: function() {
        this.oView = this.getView();
        this.oComponent = sap.ui.component(sap.ui.core.Component.getOwnerIdFor(this.oView));
        this.oResourceBundle = this.oComponent.getModel(this.oComponent.i18nModelName).getResourceBundle();
        this.oRouter = this.oComponent.getRouter();
        this.oRouter.attachRoutePatternMatched(this.onRoutePatternMatched, this);
    },

    	onRoutePatternMatched: function(oEvent) {
    		
			// Register the view with the message manager
    		this.bvtyp;
			sap.ui.getCore().getMessageManager().registerObject(this.getView(), true);
			var oMessagesModel = sap.ui.getCore().getMessageManager().getMessageModel();
			this._oBinding = new sap.ui.model.Binding(oMessagesModel, "/", oMessagesModel.getContext("/"));
			this._oBinding.attachChange(function(oEvent) {
				var aMessages = oEvent.getSource().getModel().getData();
				for (var i = 0; i < aMessages.length; i++) {
					if (aMessages[i].type === "Error" && !aMessages[i].technical) {
						that._oViewModel.setProperty("/enableCreate", false);
					}
				}
			});    		
    		 
    		this.sPageName = oEvent.getParameter("name");
    		 	
    		/*this.oPersDlg = this.byId("idPersDlg2");
    		this.persList = [];
    		this.selectedPers;
    		this.persType();*/
    		this._oViewModel;
    		this.shsatici = [];
    		this.shwaers  = [];
    		this.shsaticibanka  = [];    		
    		if (this.sPageName === "SOGirisCreate") {
    		this.clearCreate();
            this.getEventBus().publish("SOGiris", "SelectElementOtherPage", "");
            this.setPageData();
           
        }
    			
    	    	
    },
    clearCreate: function(){
    	var t = this;
    	 var oCollectionList = ["idSaticiNoInput","idName1Input", "idName2Input", "idStcd2Input", "idWaersInput","idBakiye1Input",
    		 "idBakiye2Input","idBakiye3Input","idBakiye4Input","idIbanInput","idTalepTutariInput","idIslemAciklamaInput"]; // create listesinin dataları
    	 
    	 this.clear(oCollectionList, null);
    },
    
    clear: function(sInput, sSelect){
    	var t = this;
    	if(sInput !== null){
    		for(var i = 0; i < sInput.length; i++ ){
        		this.byId(sInput[i]).setValue("");
        	}
    	}
    	
    	
    }, 
    //------------------------------- Busy Indicator ----------------------------------------------//
    _showBusy: function() {
        this.getEventBus().publish("SOGiris", "ShowBusy", null);
    },

    _hideBusy: function() {
        this.getEventBus().publish("SOGiris", "HideBusy", null);
    },
    //------------------------------- Busy Indicator ----------------------------------------------//
    
//************* 
    		setPageData: function(){
    			var t = this;
    			// Satıcı Listesi Cekelim
    			this._showBusy();
    	        var sPath = "/ShSaticiSet";
    	        this.oView.getModel().read(sPath, null, null, true,
    	            function (oData, oResponse) {
    	                var oModel = new sap.ui.model.json.JSONModel({
    	                	SOGirisCollection: []
    	                });

    	                t.shsatici = oData.results;
    	                oModel.setProperty("/SOGirisCollection", t.shsatici);
    	                t.oView.setModel(oModel, "SOGirisModel");

    	                t._oViewModel = t.getView().getModel("SOGirisModel");

    	                t._hideBusy();
    	            }, function (err) {
    	            	t._hideBusy();
    	            });   	        

    			// Parabirimi Listesi Cekelim
    			this._showBusy();
    	        var sPath = "/ShWaersSet";
    	        this.oView.getModel().read(sPath, null, null, true,
    	            function (oData, oResponse) {
    	                var oModel = new sap.ui.model.json.JSONModel({
    	                	SOGirisWaersCollection: []
    	                });

    	                t.shwaers = oData.results;
    	                oModel.setProperty("/SOGirisWaersCollection", t.shwaers);
    	                t.oView.setModel(oModel, "SOGirisWaersModel");

    	                t._hideBusy();
    	            }, function (err) {
    	            	t._hideBusy();
    	            });   	        

    	        // Satıcı Banka ve Para Birimi Turune Gore cekelim
    			this._showBusy();
    	        var sPath = "/ShSaticiBankaSet";
    	        this.oView.getModel().read(sPath, null, null, true,
    	            function (oData, oResponse) {
//    	        	
//    	        	 oData.results.unshift({
//    	                    Waers: "",
//    	                    Ltext: "Lütfen Seçiniz"
//    	                }); 
    	                var oModel = new sap.ui.model.json.JSONModel({
    	                	SOGirisSaticiBankaCollection: []
    	                });

    	                t.shsaticibanka = oData.results;
    	                oModel.setProperty("/SOGirisSaticiBankaCollection", t.shsaticibanka);
    	                t.oView.setModel(oModel, "SOGirisSaticiBankaModel");

    	                t._hideBusy();
    	            }, function (err) {
    	            	t._hideBusy();
    	            }); 
    	        
    		},
    
    		handleNavButtonPress: function() {
    			cus.ESAN.FioSOGiris.utils.Util.onNavBack(-1);
    		},
    		
    
    		clearForm: function() {

    		},
    		
    		 getEventBus: function() {
                 return sap.ui.getCore().getEventBus();
             },

             
//  <-- > Begin Of  Satıcılar ve tanımlarının secimi için yazıldı
             
            onValueHelpSaticiRequest: function() {
                this.handleOpenDialog();
            },

            handleOpenDialog: function() {
                var t = this;

                if (!this._oDialog) {
                    this._oDialog = sap.ui.xmlfragment("cus.ESAN.FioSOGiris.view.fragment.SaticiDialog", this);
                }

                oModel = new sap.ui.model.json.JSONModel();
                oModel.setProperty("/SOGirisCollection", this.shsatici);
                this._oDialog.setModel(oModel, "SOGirisModel");
                this._oDialog.open();
               // this._waersSearch("Waers");                
            },
            
            pressSaticiSearch: function() {
                this._saticiSearch("idSaticiNo", "idName1", "idName2", "idStcd2");
            },

            
            _saticiSearch: function(helpInput, name1) {

                var t = this;
                var searchinput = t.byId(helpInput).getValue();
                var sPath = "ShSaticiSet('" + searchinput + "')";

                this.oView.getModel().read(sPath, null, null, true,
                    function(oData, oResponse) {
                        t.byId(helpInput).setValue(oData.Lifnr);
                        t.byId(name1).setValue(oData.Name1);
                        t.byId(name2).setValue(oData.Name2);
                        t.byId(stcd2).setValue(oData.Stcd2);
                    },
                    function(error) {
                        sap.m.MessageToast.show(getText("Kayıt bulunamadı"));
                    });
            },

            //*** Filtrenin calısması için yazıldı.
            filterSaticiListLive: function(oEvent) {
            	var value = oEvent.getParameter("value");
                var oF1 = new sap.ui.model.Filter("Lifnr", sap.ui.model.FilterOperator.Contains, value);
                var oF2 = new sap.ui.model.Filter("Name1", sap.ui.model.FilterOperator.Contains, value);
                var oF3 = new sap.ui.model.Filter("Name2", sap.ui.model.FilterOperator.Contains, value);                
                var oF4 = new sap.ui.model.Filter("Stcd2", sap.ui.model.FilterOperator.Contains, value);
                var oFilters = new sap.ui.model.Filter({
                    filters: [
                        oF1,
                        oF2,
                        oF3,
                        oF4
                    ],
                    and: false
                });
                oEvent.getSource().getBinding("items").filter(oFilters, sap.ui.model.FilterType.Application);
              },  


              handleConfirm: function(oEvent) {
                  if(oEvent.sId === "confirm"){
                      
                      this.byId("idSaticiNoInput").setValue(oEvent.mParameters.selectedItem.getCells()[0].mProperties.title);
                      this.byId("idName1Input").setValue(oEvent.mParameters.selectedItem.getCells()[1].mProperties.title);
                      this.byId("idName2Input").setValue(oEvent.mParameters.selectedItem.getCells()[2].mProperties.title); 
                      this.byId("idStcd2Input").setValue(oEvent.mParameters.selectedItem.getCells()[3].mProperties.title);                     
                  }
                  this.byId("idWaersInput").setEnabled(true);
                        
              },      
              
//  <-- > End Of          ************  ************   ************   ************   ************   ************ 
              

//  <-- > Begin Of  Parabirimi ve tanımlarının secimi için yazıldı
              
            onValueHelpWaersRequest: function() {
                this.handleWaersOpenDialog();
            },

            handleWaersOpenDialog: function() {
                var t = this;

                if (!this._oDialogWaers) {
                    this._oDialogWaers = sap.ui.xmlfragment("cus.ESAN.FioSOGiris.view.fragment.WaersDialog", this);
                }

                oModel = new sap.ui.model.json.JSONModel();
                oModel.setProperty("/SOGirisWaersCollection", this.shwaers);
                this._oDialogWaers.setModel(oModel, "SOGirisWaersModel");
                this._oDialogWaers.open();             
            },
            
            pressWaersSearch: function() {
                this._waersSearch("idWaers", "idLtext");
            },
            
            _waersSearch: function(helpInput, ltext) {

                var t = this;
                var searchinput = t.byId(helpInput).getValue();
                var sPath = "ShWaersSet('" + searchinput + "')";

                this.oView.getModel().read(sPath, null, null, true,
                    function(oData, oResponse) {
                        t.byId(helpInput).setValue(oData.Waers);
                        t.byId(ltext).setValue(oData.Ltext);
                    },
                    function(error) {
                        sap.m.MessageToast.show(getText("Para Birimleri Bulunamadı"));
                    });
            },

            //*** Filtrenin calısması için yazıldı.
            filterWaersListLive: function(oEvent) {
            	var value = oEvent.getParameter("value");
                var oF1 = new sap.ui.model.Filter("Waers", sap.ui.model.FilterOperator.Contains, value);
                var oF2 = new sap.ui.model.Filter("Ltext", sap.ui.model.FilterOperator.Contains, value);
                var oFilters = new sap.ui.model.Filter({
                    filters: [
                        oF1,
                        oF2,
                    ],
                    and: false
                });
                oEvent.getSource().getBinding("items").filter(oFilters, sap.ui.model.FilterType.Application);
              },  
              
              handleConfirmWaers: function(oEvent) {
                  if(oEvent.sId === "confirm"){   
                      var waers = this.byId("idWaersInput").setValue(oEvent.mParameters.selectedItem.getCells()[0].mProperties.title); 
                      this.getbakiyedetay();  
                      this.getibandetay();                        
                      }
//                  this.byId("idWaersInput").setEnabled(false);
                  this.byId("idIbanInput").setEnabled(true);
              } ,   

          getbakiyedetay: function() {
  			this._showBusy();
  			var t = this;
  			this.oView.getModel().callFunction("/BakiyeDetayFunc", {
  				urlParameters: {
  					Satici: t.byId("idSaticiNoInput").getValue(),
  					Waers: t.byId("idWaersInput").getValue()
            },
            success: function(oData, Response) {
            	t.byId("idBakiye1Input").setValue(oData.Bakiye1);
            	t.byId("idBakiye2Input").setValue(oData.Bakiye2);
            	t.byId("idBakiye3Input").setValue(oData.Bakiye3);            	
           	  	t._hideBusy();
            },
            error: function(error) {
                t._hideBusy();
            },
  			}); 
              },
                          
            	  
 //  <-- > End Of          ************  ************   ************   ************   ************   ************ 
            
 //  <-- > Begin Of  Satıcıya ait Banka ve Para Birimi secimi için yazıldı
              
            onValueHelpSaticiBankaRequest: function() {
                this.handleSaticiBankaOpenDialog();
            },

            handleSaticiBankaOpenDialog: function() {
                var t = this;

                if (!this._oDialogSaticiBanka) {
                    this._oDialogSaticiBanka = sap.ui.xmlfragment("cus.ESAN.FioSOGiris.view.fragment.SaticiBankaDialog", this);                    
                }

                oModel = new sap.ui.model.json.JSONModel();
                oModel.setProperty("/SOGirisSaticiBankaCollection", this.shsaticibanka);
                this._oDialogSaticiBanka.setModel(oModel, "SOGirisSaticiBankaModel");
                this._oDialogSaticiBanka.open();
            },
            
            pressSaticiBankaSearch: function() {
                this._saticibankaSearch("idIban", "idWaersIban","idBvtypIban");
            },

            _saticibankaSearch: function(helpInput, waers,bvtyp) {

                var t = this;
                var searchinput = t.byId(helpInput).getValue();
                var sPath = "ShSaticiBankaSet('" + searchinput + "')";

                this.oView.getModel().read(sPath, null, null, true,
                    function(oData, oResponse) {
                        t.byId(helpInput).setValue(oData.Iban);
                        t.byId(waers).setValue(oData.Waers);
                        t.byId(bvtyp).setValue(oData.Bvtyp);                        
                    },
                    function(error) {
                        sap.m.MessageToast.show(getText("IBAN bilgisi bulunamadı"));
                    });
            },
            
            //*** Filtrenin calısması için yazıldı.
            filterSaticiBankaListLive: function(oEvent) {
            	var value = oEvent.getParameter("value");
                var oF1 = new sap.ui.model.Filter("Iban", sap.ui.model.FilterOperator.Contains, value);
                var oF2 = new sap.ui.model.Filter("Waers", sap.ui.model.FilterOperator.Contains, value);
                var oF3 = new sap.ui.model.Filter("Bvtyp", sap.ui.model.FilterOperator.Contains, value);                
                var oFilters = new sap.ui.model.Filter({
                    filters: [
                        oF1,
                        oF2,
                        oF3,
                    ],
                    and: false
                });
                oEvent.getSource().getBinding("items").filter(oFilters, sap.ui.model.FilterType.Application);
              },  
              

              handleConfirmSaticiBanka: function(oEvent) {
            	  this.bvtyp = oEvent.mParameters.selectedItem.mAggregations.cells[2].mProperties.title;
                  if(oEvent.sId === "confirm"){
                      
                      this.byId("idIbanInput").setValue(oEvent.mParameters.selectedItem.getCells()[0].mProperties.title);                    
                  }
                  this.byId("idIbanInput").setEnabled(false);
                        
              },  

            getibandetay: function() {
  			this._showBusy();
  			var t = this;
  			this.oView.getModel().callFunction("/GetIban", {
  				urlParameters: {
  					Satici: t.byId("idSaticiNoInput").getValue(),
  					Waers: t.byId("idWaersInput").getValue()
            },
            success: function(oData, Response) {
                oModel = new sap.ui.model.json.JSONModel();
                oModel.setProperty("/SOGirisSaticiBankaCollection", oData.results);
                t.shsaticibanka = oData.results;
             	t._hideBusy();
            },
            error: function(error) {
                t._hideBusy();
            },
        }); 
              }, 
//  <-- > End Of  Satıcıya ait Banka ve Para Birimi secimi için yazıldı
              
              
 //  <-- > Begin Of  Odeme talebini kayıt edelim
                      
              handleSaveButton: function() {
                  var t = this;
            	  if (this.byId("idSaticiNoInput").getValue() == "" ) {  sap.m.MessageToast.show("Satici Girişi Yapınız.") ; return;}  
            	  if (this.byId("idStcd2Input").getValue() == "" ) {  sap.m.MessageToast.show("Vergi No Bakımı Yapınız.") ; return;}
            	  if (this.byId("idWaersInput").getValue() == "" ) {  sap.m.MessageToast.show("Para Birimi Bakımı Yapınız.") ; return;}
            	  if (this.byId("idIbanInput").getValue() == "" ) {  sap.m.MessageToast.show("Satici Banka Bakımı Yapınız.") ; return;}   
            	  
            	  // Bakiyelere gore talep tutarı kontrolu yapılacak
            	  if (this.byId("idBakiye1Input").getValue() == "" && 
            	  	  this.byId("idBakiye2Input").getValue() == "" && 
            	  	  this.byId("idTalepTutariInput").getValue() !== "")  
            	  {  sap.m.MessageToast.show("Satıcıya ait bakiye bulunamadı.Talep girişi yapılamaz..") ; return;}
            	 
            	 var lv_bakiye_check; 
            	 var lv_bakiye1 =  this.byId("idBakiye1Input").getValue();
            	 var lv_taleptutari = this.byId("idTalepTutariInput").getValue(); 
            	 if (lv_taleptutari <= 0.00 )
             	  {  sap.m.MessageToast.show("Talep tutarı girişİ yapınız.") ; return;}

            	 var lv_bakiye3 = this.byId("idBakiye3Input").getValue();
            	 lv_bakiye_check = lv_bakiye1 - lv_bakiye3;
            	 if (lv_bakiye_check < lv_taleptutari)
              	  {  sap.m.MessageToast.show("Talep tutarı,Vadesi gelen bakiye ve eski taleplerin toplamından küçük olmalıdır,Lütfen Avans sayfasından giriş yapınız.") ; return;}
            		 
            	  
            	  // İşlem acıklaması icin konusulacak
//            	  if (this.byId("idIslemAciklamaInput").getValue() == "" )
//            	  { sap.m.MessageBox.alert("**Opsiyonel-İşlem Açıklama Girişi.") ; return;}
            	  
            	  this._showBusy();
                  var saveSet = {
                      Statu: "GIRIS",
                      Satici:  this.byId("idSaticiNoInput").getValue(),
                      Name1:   this.byId("idName1Input").getValue(),
                      Name2:   this.byId("idName2Input").getValue(),                       
                      Waers:   this.byId("idWaersInput").getValue(), 
                      Stcd2:   this.byId("idStcd2Input").getValue(),                                  
                      Bakiye1: this.byId("idBakiye1Input").getValue(),
                      Bakiye2: this.byId("idBakiye2Input").getValue(),
                      Bakiye3: this.byId("idBakiye3Input").getValue(),   
                      Bakiye4: this.byId("idBakiye3Input").getValue(),                        
                      Iban:    this.byId("idIbanInput").getValue(),
                      Taleptutari:   this.byId("idTalepTutariInput").getValue(),
                      Islmacik: this.byId("idIslemAciklamaInput").getValue(),
                      //Odemetipi: "",
                      //Odemeno: "",
                      //Waerk: "",
                      //Odeyenbanka: "",
                      //Odeyenbankaiban: "",
                      //Yoneticinotu: "",
                      //Finansnotu: "",
                      Onaylanantutar: "0.00", 
                      //Belnr: "",
                      //Bukrs: "",
                      //Gjahr: "",
                      //Uname: "",
                      //Datum:"",
                      //Uzeit: "",
                      Zreturn:"",
                      Zmessage:"",
                      Bvtyp: t.bvtyp,
                      Odmbcm:"F",
                      //Talepno:"",
                      //Bankulke:"",
                      //Bankanah:"",
                      //Bankhesn:"",
                  };
                  var oModel = this.oView.getModel();
                  var batch = oModel.createBatchOperation("/SaticiOdemeGirisSet", "POST", saveSet);
                  var batchList = [batch]; 
                  cus.ESAN.FioSOGiris.utils.Util.sendBatchChangeOperations(oModel, batchList, 
                 function(s) {
                      if (s[0].Zreturn === "SUCCESS") {
                    	  t._hideBusy();
                    	 // t.oResourceBundle.getText("I18TANIMI"),
                    	  sap.m.MessageBox.alert(s[0].Zmessage);
                    	  t.oRouter.navTo("SOGirisDetail", {
                                 from: "SOGirisCreate",
                                 Odemeno: s[0].Odemeno
                             });
                      }},
                 function(e) {
                      t._hideBusy();
                      sap.m.MessageBox.alert(e.message.value);
                  });
             
              },
                      
//  <-- > End Of  Odeme talebini kayıt edelim              

        
//  <-- > Begin Of Tutar alan kontrolu       
      		_validateSaveEnablement: function() {
    			var aInputControls = this._getFormFields(this.byId("newEntitySimpleForm"));
    			var oControl;
    			for (var m = 0; m < aInputControls.length; m++) {
    				oControl = aInputControls[m].control;
    				if (aInputControls[m].required) {
    					var sValue = oControl.getValue();
    					if (!sValue) {
    						this._oViewModel.setProperty("/enableCreate", false);
    						return;
    					}
    				}
    			}
    			this._checkForErrorMessages();
    		},

    		_getFormFields: function(oSimpleForm) {
    			var aControls = []; var sInput = [];
    			var aFormContent = oSimpleForm.getFormContainers()[0].getFormElements();
//    			oSimpleForm.getFormContainers()[0].a.getFormElements()[9].getLabel().__sLabeledControl
    			var sControlType;
    			for (var i = 0; i < aFormContent.length; i++) {
    				sControlType = this.byId(aFormContent[i].getLabel().__sLabeledControl).getMetadata().getName();
    				
    				sInput.push(this.byId(aFormContent[i].getLabel().__sLabeledControl));
    				if (sControlType === "sap.m.Input" || sControlType === "sap.m.DateTimeInput" ||
    					sControlType === "sap.m.CheckBox") {
    					aControls.push({
    						control: sInput[i],
    						required: sInput[i].getRequired && sInput[i].getRequired()
    					});
    				}
    			}
    			return aControls;
    		},
    		
    		_checkForErrorMessages: function() {
    			var aMessages = this._oBinding.oModel.oData;
    			if (aMessages.length > 0) {
    				var bEnableCreate = true;
    				for (var i = 0; i < aMessages.length; i++) {
    					if (aMessages[i].type === "Error" && !aMessages[i].technical) {
    						bEnableCreate = false;
    						break;
    					}
    				}
    				
    				this._oViewModel.setProperty("/enableCreate", bEnableCreate);
    			} else {
    				this._oViewModel.setProperty("/enableCreate", true);
    			}
    		},   		
//  <-- > End Of Tutar alan kontrolu 
    		
        /*    handleAcceptPers: function() {
                if(this.screen === "Create") {
                    this.byId("idSicilNo").setValue(this.selectedPers[0].getTitle());
                    this.byId("idName").setValue(this.selectedPers[1].getText());
                    this.byId("idSurname").setValue(this.selectedPers[2].getText());
                
                }
                this.oPersDlg.close();
                this.oPersDlg.setVisible(false);

                var a = this.byId("idPersSearch").setValue("");
                var oBinding = this.byId("idWorkOrderTable").getBinding("items");
                oBinding.filter();
            },
            handleRejectPers: function() {
                this.selectedSicilNo = "";
                this.oPersDlg.close();
                this.oPersDlg.setVisible(false);

                var a = this.byId("idPersSearch").setValue("");
                var oBinding = this.byId("idWorkOrderTable").getBinding("items");
                oBinding.filter();
            },
            
            getSplitAppObj: function() {
                var result = this.byId("SplitAppDemo");

                if (!result) {
                    jQuery.sap.log.info("SplitApp object can't be found");
                }
                return result;
            },*/
            
});
