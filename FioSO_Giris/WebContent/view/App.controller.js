sap.ui.controller("cus.ESAN.FioSOGiris.view.App", {
    onInit: function() {
        this.oView = this.getView();

        var oEventBus = this.getEventBus();
        oEventBus.subscribe("SOGiris", "ShowHideMaster", this.showHideMaster, this);
    },

    showHideMaster: function(sChannel, sEvent, sType) {
        var oApp = this.byId("fioriContent");
        if (sType === "show") {
            oApp.showMaster();
        } else {
            oApp.hideMaster();
        }
    },

    getEventBus: function() {
        return sap.ui.getCore().getEventBus();
    },

    onExit: function() {
        var oEventBus = this.getEventBus();
        oEventBus.unsubscribe("SOGiris", "ShowHideMaster", this.showHideMaster, this);
    },
});