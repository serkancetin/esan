sap.ui.controller("cus.ESAN.FioSOGiris.view.SOGirisMaster", {
    onInit: function() {
        this.oView = this.getView();
        this.oComponent = sap.ui.component(sap.ui.core.Component.getOwnerIdFor(this.oView));
        this.oResourceBundle = this.oComponent.getModel(this.oComponent.i18nModelName).getResourceBundle();
        this.oRouter = this.oComponent.getRouter();
 
        var oEventBus = this.getEventBus();
        oEventBus.subscribe("SOGiris", "SelectElementOtherPage", this.selectElementOtherPage, this);
        oEventBus.subscribe("SOGiris", "RefreshModel", this.refreshModel, this); 
        oEventBus.subscribe("SOGiris", "ShowBusy", this._showBusy, this);
        oEventBus.subscribe("SOGiris", "HideBusy", this._hideBusy, this);  
        
        this.oRouter.attachRoutePatternMatched(this.onRoutePatternMatched, this); 
    },
 
    onRoutePatternMatched: function(oEvent) {
    	var t = this;
    	
        var oCollectionList = ["StatuListSet", "SaticiListSet"];     
        this.setComboModel(oCollectionList);
    	this.sPageName = oEvent.getParameter("name");
        this.oList = this.byId("idSOGirisList");
        this.searchFragmentId = "searchFragment";
        this.busyCount = 0;
        SearchfragmentId = this.getView().createId(this.searchFragmentId);
      
        if (this.sPageName === "SOGiris") {
            this.oView.setModel(this.oComponent.getModel());
            
            this.byId("searchDialog").open();
          //Ekipman uygulamasından gelme durumuna karşı startup parameters kontrol ediliyor
            if (this.oComponent.getComponentData() && !this.startupLoaded) {
                this.startupParams = this.oComponent.getComponentData().startupParameters;
                if (this.startupParams) {
                     if (this.startupParams.Odemeno) {
                        this.startupLoaded = true;
                        this._setFilter(null, null, this.startupParams.Odemeno[0]);
                    } else 
                     if (this.startupParams.Odemeno) {
                        this.startupLoaded = true;
                        this.oRouter.navTo("SOGirisDetail", {
                            from: "SOGiris",
                            Odemeno: this.startupParams.Odemeno[0]
                        });
                    } 
                     else {
                        this.bindSOGirisList();
                    }
                }
                else {
                    this.bindSOGirisList();
                }
            }
        }
    },
 
    handleNavButtonPress: function() {
        cus.ESAN.FioSOGiris.utils.Util.onNavBack(-1);
    },
    
    onNavBack: function() {
        cus.ESAN.FioSOGiris.utils.Util.onNavBack(-1);
    },
    
    onUpdateFinished: function(oEvent) {
        this.setItemsColor();
    },
    
    setItemsColor: function() {
        var oItems = this.byId("idSOGirisList").getItems();
        for (i = 0; i < oItems.length; i++) {
            var oTitle = oItems[i]._oTitleText;
            if (oTitle.addStyleClass != undefined) {
                 
                    oTitle.addStyleClass("fieldRed");
                
            }
        }
    },
    
 
    bindSOGirisList: function(sOdemeno) { 
     
        var t = this;
        this._showBusy();
       var oFilters = this._getFilter();
        
        this.oView.getModel().read( "/SaticiOdemeGirisSet", {
           filters: [oFilters],
            success: function(oData, oResponse) {
                 t._bindSOGirisListWithOdata(sOdemeno, oData);
                t._hideBusy();
                if (t.byId("searchDialog"))
                    t.byId("searchDialog").close();
            
            },
            error: function(err) {
                t._hideBusy();
               
            }
        }); 
    },
    
 // oModele Odata set edilir. 
    _bindSOGirisListWithOdata: function(sOdemeno, oData) {
        var t = this;
        var oModel = new sap.ui.model.json.JSONModel({
            SOGirisCollection: []
        });
        oModel.setProperty("/SOGirisCollection", oData.results);
        t.oView.setModel(oModel, "SOGirisGroupsModel");
        t.selectElement(sOdemeno);
    },

    setComboModel: function(oCollectionList) {
        var t = this;

        for (i = 0; i < oCollectionList.length; i++) {
        	this._showBusy();
           
            this.oView.getModel().read("/" + oCollectionList[i], null, null, true,
                function(oData, oResponse) {
                    var uriParse = oResponse.requestUri.split("/");
                    for (k = 0; k < uriParse.length; k++) {
                        t.getName(oCollectionList, uriParse[k], function(name) {
                            if (name) {
                                var oModel = t.oView.getModel("SelectItemModel");
                                if (!oModel) {
                                    oModel = new sap.ui.model.json.JSONModel();
                                }
                                oModel.setProperty("/" + name + "Collection", oData.results);
                                t.oView.setModel(oModel, "SelectItemModel");
                            }
                        });
                    }
                    t._hideBusy();
                },
                function(err) {
                	t._hideBusy();
                    sap.m.MessageBox.show(this.oResourceBundle.getText("SYSTEM_ERROR"));
                });
        }
    },
    
    getName: function(oCollectionList, value, _callback) {
        for (i = 0; i < oCollectionList.length; i++) {
            if (value === oCollectionList[i])
                _callback(oCollectionList[i]);
        }
        _callback(undefined);
    },
    
    selectElement: function(sOdemeno) {
        if (!sap.ui.Device.system.phone) {
            var aItems = this.oList.getItems();
            if (sOdemeno) {
                for (var i = 0; i < aItems.length; i++) {
                    if (aItems[i].getBindingContext("SOGirisGroupsModel").getProperty("Odemeno") === sOdemeno) {
                        this.oList.setSelectedItem(aItems[i], true);
                        break;
                    }
                }
            } else if (aItems.length && !this.oList.getSelectedItem()) {
                this.oList.setSelectedItem(aItems[0], true);
                this.showDetail(aItems[0]);
            }
        }
    },
 
    selectElementOtherPage: function(sChannel, sEvent, sOdemeno) {
        if (!sap.ui.Device.system.phone) {
            var aItems = this.oList.getItems();
            if (aItems.length === 0) {
                this.bindSOGirisList(sOdemeno);
            } else {
                this.selectElement(sOdemeno);
            }
        }
    },
 
    refreshModel: function(sChannel, sEvent, sOdemeno) {
        this.bindSOGirisList(sOdemeno);
    },
    
    
    _setFilter: function(sEqunr) {
        this.bindSOGirisList();
   },
   handleSearchButtonPress: function(){ 
	   this.bindSOGirisList();  
   },
// Search Help Secilen Verileri Filter'a doldur
_getFilter: function() {
        var filters = [];
        var filterCount = 0;
 

 
        // VendorGet...For Multiple Value------------------------------------------------------------//
        var oSatici = sap.ui.core.Fragment.byId(SearchfragmentId, "idSaticiList");
        var saticiList = oSatici.getSelectedKeys();
        if (saticiList.length > 0) {
            for (var i = 0; i < saticiList.length; i++) {
                var oFilter1 = new sap.ui.model.Filter("Satici", sap.ui.model.FilterOperator.EQ, saticiList[i]);
                filters.push(oFilter1);
            }
            filterCount++;
        }
 
        // StatusGet...For Multiple Value------------------------------------------------------------//
        var oStatu = sap.ui.core.Fragment.byId(SearchfragmentId, "idStatuList");
        var statuList = oStatu.getSelectedKeys();
        if (statuList.length > 0) {
            for (var i = 0; i < statuList.length; i++) {
                var oFilter2 = new sap.ui.model.Filter("Statu", sap.ui.model.FilterOperator.EQ, statuList[i]);
                filters.push(oFilter2);
            }
            filterCount++;
        }
    
//        // Başlangıç Tarihi... ----------------------------------------------------//
//        var oDate = sap.ui.core.Fragment.byId(SearchfragmentId, "idTalepTarihi");
//        if (oDate.getValue() !== "") {
//            var fromDate = cus.ESAN.FioSOGiris.utils.formatter.dateFormatToOdata(oDate.getFrom());
//            var toDate = cus.ESAN.FioSOGiris.utils.formatter.dateFormatToOdata(oDate.getTo());
//            var oFilter = new sap.ui.model.Filter("Taleptarihi", sap.ui.model.FilterOperator.BT, fromDate, toDate);
//            filters.push(oF4);
//            filterCount++;
//        }
 
        if (filterCount > 0) {
            this._addBadge("idFilterButton", "", filterCount.toString());
        } else {
            this._removeBadge("idFilterButton");
        }
 
        return new sap.ui.model.Filter({
            filters: filters,
            and: true
        });
    },
 
   //---SearchField Begin---// 
    onLiveChange: function(oEvent) {
        if (oEvent.getParameters().refreshButtonPressed) {
            var oItem = this.oList.getSelectedItem();
            if (oItem) {
                var bindingContext = oItem.getBindingContext("SOGirisGroupsModel");
                var sOdemeno = bindingContext.getProperty("Odemeno");
                this.bindSOGirisList(sOdemeno);
            }
        }
        var value = oEvent.getSource().getValue();
        var oF1 = new sap.ui.model.Filter("Satici", sap.ui.model.FilterOperator.Contains, value);
        var oF2 = new sap.ui.model.Filter("Statu", sap.ui.model.FilterOperator.Contains, value);
        var oF3 = new sap.ui.model.Filter("Taleptarihi", sap.ui.model.FilterOperator.Contains, value);
        var oF4 = new sap.ui.model.Filter("Odemetipi", sap.ui.model.FilterOperator.Contains, value);
        var oFilters = new sap.ui.model.Filter({
            filters: [
                oF1,
                oF2,
                oF3,
                oF4
            ],
            and: false
        });
        this.oList.getBinding("items").filter(oFilters, sap.ui.model.FilterType.Application);
    },
    
    handleOpenDialogMaster: function() {
        
    },
    
    //SearchField End---//
    
 //---Sort Filter Begin--//
    onSort: function() {
        this.oUserSortDlg._getDialog().getEndButton().setType("Reject");
        this.oUserSortDlg._getDialog().getEndButton().setIcon("sap-icon://decline");    
        this.oUserSortDlg._getDialog().getBeginButton().setType("Accept");    
        this.oUserSortDlg._getDialog().getBeginButton().setIcon("sap-icon://accept");        
        this.oUserSortDlg._getDialog().getEndButton().setText("İPTAL");    
        this.oUserSortDlg.open();
    },
    handleConfirmSOGirisSort: function(oEvent) {
        var key = oEvent.mParameters.sortItem.getKey();
        var sortkey = "";
        switch (key) {
            case "1":
                sortkey = "Satici";
                break;
            case "2":
                sortkey = "Statu";
                break;
            case "3":
                sortkey = "Taleptarihi";
                break;
            case "4":
                sortkey = "Odemetipi";
                break;
 
        }
        var descending = oEvent.mParameters.sortDescending;
        var oBinding = oList.getBinding("items");
        var group = false;
        var aSorter = [];
 
        aSorter.push(new sap.ui.model.Sorter(sortkey, descending, group));
        oBinding.sort(aSorter);
        
    },
    //---Sort Filter End---//
        
 
    onNavigationItemPressed: function(oEvent) {
        this.showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
    },
    // Seçilen Item'i soldan saga at.
    showDetail: function(oItem) {
        var bindingContext = oItem.getBindingContext("SOGirisGroupsModel");
        var sOdemeno = bindingContext.getProperty("Odemeno");
        this.oRouter.navTo("SOGirisDetail", {
            from: "SOGiris",
            Odemeno: sOdemeno
        });
    },
 
    getEventBus: function() {
        return sap.ui.getCore().getEventBus();
    },
 
    //---UserCreate Navigation Begin---//
    onPressNavToNew: function() {
        this.oList.removeSelections(true);
        this.oRouter.navTo("SOGirisCreate", {
            from: "SOGiris",
        });
    },
  //---UserCreate Navigation End---//
    
    onExit:function(){
        var oEventBus = this.getEventBus();
        oEventBus.unsubscribe("SOGiris", "SelectElementOtherPage", this.selectElementOtherPage, this);
        oEventBus.unsubscribe("SOGiris", "RefreshModel", this.refreshModel, this);
       // oEventBus.unsubscribe("SOGiris", "DeleteUser", this.DeleteUser, this);
        oEventBus.unsubscribe("SOGiris", "ShowBusy", this._showBusy, this);
        oEventBus.unsubscribe("SOGiris", "HideBusy", this._hideBusy, this); 
    },
    
    _showBusy: function() {
        this.busyCount += 1;
        sap.ui.core.BusyIndicator.show();
    },
 
    _hideBusy: function() {
        this.busyCount -= 1;
        if (this.busyCount <= 0) {
            this.busyCount = 0;
            sap.ui.core.BusyIndicator.hide();
        }
    },
    
/*    DeleteUser: function(sChannel, sEvent, sUname) {
        var items = this.byId("idUserList").getItems();
        for (i = 0; i < items.length; i++) {
            if (items[i].mProperties.description === sUname) {
                this.byId("idUserList").removeItem(i);
 
                break;
            }
        }
        items = this.byId("idUserList").getItems();
        if (!sap.ui.Device.system.phone) {
            if (items.length === 0) {
                this.bindSOGirisList(items[i].mProperties.description);
            } else {
                this.selectElement(items[i].mProperties.description);
            }
        }
        this.oRouter.navTo("UserDetail", {
            from: "SOGiris",
            Uname: items[i].mProperties.description
        });
 
    },
    */
    handleOpenDialog: function() {
        this.byId("searchDialog").open();
        //this.hideAndShowDialogItems();
    },
    
    hideAndShowDialogItems: function() {
       var oObjectTypeSelect = sap.ui.core.Fragment.byId(SearchfragmentId, "idSatici");
        oObjectTypeSelect.setVisible(false);
        oObjectTypeSelect.setVisible(true);
 
       var oObjectTypeSelect = sap.ui.core.Fragment.byId(SearchfragmentId, "idStatu");
        oObjectTypeSelect.setVisible(false);
        oObjectTypeSelect.setVisible(true);
 
       var oObjectTypeSelect = sap.ui.core.Fragment.byId(SearchfragmentId, "idTaleptarihi");
        oObjectTypeSelect.setVisible(false);
        oObjectTypeSelect.setVisible(true);
 
        // var oLocationSelect = sap.ui.core.Fragment.byId(SearchfragmentId, "idEkipmanSelect");
        // oLocationSelect.setVisible(false);
        // oLocationSelect.setVisible(true);
    },
 
    handleSearchClosePress: function() {
        this.byId("searchDialog").close();
    },
    _addBadge: function(sControlId, sType, sNumber) {
        var oButton = this.byId(sControlId);
        oButton.addStyleClass("uoMBtnBadge");
        oButton.addStyleClass(sType);
        oButton.data("badge", "", true);
        oButton.data("badge", sNumber, true);
    },
 
    _removeBadge: function(sControlId) {
        var oButton = this.byId(sControlId);
        oButton.removeStyleClass("uoMBtnBadge");
        oButton.removeStyleClass("uoBadgeInformation");
        oButton.removeStyleClass("uoBadgeSuccess");
        oButton.removeStyleClass("uoBadgeError");
        oButton.removeStyleClass("uoBadgeWarning");
    },
    
    onBackLanchpad: function() {
        if (window.location.hostname == "localhost") {
            return;
        }
        var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
        var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
            target: {
                semanticObject: '#',
            },
        })) || "";
        oCrossAppNavigator.toExternal({
            target: {
                shellHash: hash
            }
        });
    },
    
   
    
});
